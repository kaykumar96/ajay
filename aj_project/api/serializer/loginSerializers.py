from rest_framework import serializers
from rest_framework import exceptions
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
# from api.models import Iemi, IpAddress

class LoginSerializer(serializers.Serializer):
	username   = serializers.CharField(read_only=False)
	password   = serializers.CharField()

	class Meta:
		fields = ('user','password')
		model  = User

	def validate(self, data):
		username   = data.get("username","")
		password   = data.get("password","")		
		if username and password:
			user = authenticate(username=username, password=password)
			if user:
				if user.is_active:
					data['user'] = user
					msg = "Successfully login"
			else:
				msg = "Incorrect username or password"
				raise exceptions.ValidationError(msg)	
		else:
			msg = "Must provive both username and password"
			raise exceptions.ValidationError(msg)			
		return data