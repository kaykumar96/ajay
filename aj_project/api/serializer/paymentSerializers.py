from rest_framework import serializers
from api.models import Payment
from django.contrib.auth.models import User
from django.urls import reverse


class PaymentSerializer(serializers.ModelSerializer):

	class Meta:
		#fields = ('id','roll_no','email','password', 'name', 'd_o_b', 'email', 'mobile_no', 'profile_image', 'sign_image')
		fields  = '__all__'
		model = Payment

	def create(self, validated_data):
		applicant = Payment.objects.create(**validated_data)
		user = User.objects.create_user(username=validated_data['roll_no'],last_name=applicant.pk,email=validated_data['email'],password='password',is_staff=1,is_active=1)	
		return applicant	
	def update(self,instance,validated_data):
		applicant = Applicant.objects.filter(id=instance.id).update(**validated_data)
		return instance	