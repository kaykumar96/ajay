from rest_framework import serializers
from api.models import Applicant
from django.contrib.auth.models import User
from django.urls import reverse


class ApplicantPasswordSerializer(serializers.ModelSerializer):
	password = serializers.CharField(max_length=100)
	class Meta:
		fields = ('id','password')
		model = Applicant
	def update(self,instance,validated_data):
		print(validated_data)
		userupdate = User.objects.get(last_name=instance.id)
		if userupdate:
			userupdate.set_password(validated_data['password'])
			userupdate.save()
		return userupdate


	
class RegistrationSerializer(serializers.ModelSerializer):
	password          = serializers.CharField(default='null')

	class Meta:
		fields = ('id','roll_no','email','password', 'name', 'd_o_b', 'email', 'mobile_no', 'profile_image', 'sign_image')
		model = Applicant

	def create(self, validated_data):
		# print(validated_data)
		password = validated_data['password']
		validated_data.pop('password', None)
		applicant = Applicant.objects.create(**validated_data)
		user = User.objects.create_user(username=validated_data['roll_no'],last_name=applicant.pk,email=validated_data['email'],password=password,is_staff=1,is_active=1)	
		return applicant	
	def update(self,instance,validated_data):
		validated_data.pop('password', None)
		applicant = Applicant.objects.filter(id=instance.id).update(**validated_data)
		return instance	


class ApplicantSerializer(serializers.ModelSerializer):

	class Meta:
		#fields = ('id','roll_no','email','password', 'name', 'd_o_b', 'email', 'mobile_no', 'profile_image', 'sign_image')
		fields  = '__all__'
		model = Applicant

	def create(self, validated_data):
		applicant = Applicant.objects.create(**validated_data)
		user = User.objects.create_user(username=validated_data['roll_no'],last_name=applicant.pk,email=validated_data['email'],password='password',is_staff=1,is_active=1)	
		return applicant	
	def update(self,instance,validated_data):
		applicant = Applicant.objects.filter(id=instance.id).update(**validated_data)
		return instance	


	
