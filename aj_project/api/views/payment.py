from rest_framework import generics
from api.models import Payment
from api.serializer.paymentSerializers import PaymentSerializer
from django.db.models import Prefetch
from django.db.models import Q
import json


class list(generics.ListCreateAPIView):
	serializer_class = PaymentSerializer
	queryset = Payment.objects.all()		
	def get_queryset(self):		
		queryset = Payment.objects.all()
		return queryset