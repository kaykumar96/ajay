# from rest_framework import generics
# from api.models import Employee
from api.serializer.loginSerializers import LoginSerializer
# from django.db.models import Prefetch
# from django.db.models import Q
from rest_framework.views import APIView
from django.contrib.auth import login as django_login, logout as django_logout

from requests import get

class login(APIView):
	def post(self, request):
		self.request.POST._mutable = True	
		serializer = LoginSerializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		#message = serializer.validated_data['message']
		user = serializer.validated_data["user"]
		django_login(request, user)
		
		


		#user_instance = User.objects.get(id=token.user_id)
		return Response({"user_id":user.pk},status=200)

# class logout(APIView):
# 	authentication_classes = (TokenAuthentication,)
# 	def post(self, request):
# 		django_logout(request)
# 		client.logout()
# 		return Response(status=204)		
	