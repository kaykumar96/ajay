from rest_framework import generics
from api.models import Applicant
from api.serializer.applicationSerializers import ApplicantSerializer, ApplicantPasswordSerializer, RegistrationSerializer
from django.db.models import Prefetch
from django.db.models import Q
import json

class list(generics.ListCreateAPIView):
	serializer_class = RegistrationSerializer
	queryset = Applicant.objects.all()		
	#authentication_classes = [TokenAuthentication]


	def get_queryset(self):
		""" allow rest api to filter by submissions """
		
		queryset          = Applicant.objects.all()
# 		firm_id           = self.request.query_params.get('firm_id', None)
# 		company_id        = self.request.query_params.get('company_id',None)
# 		role_id           = self.request.query_params.get('role_id', None)
# 		role_ids          = self.request.query_params.get('role_ids', None)
# 		email             = self.request.query_params.get('email', None)
# 		mobile            = self.request.query_params.get('mobile',None)
# 		not_employee_id   = self.request.query_params.get('not_employee_id',None)
# 		search            = self.request.query_params.get('search',None)
# 		sort_by           = self.request.query_params.get('order_by',None)
# 		collumn_name      = self.request.query_params.get('field_name',None)
# 		assigned_marketer = self.request.query_params.get('assigned_marketer',None)
# 		marketers         = self.request.query_params.get('marketers',None)
# 		not_employee      = self.request.query_params.get('not_employee',None)
# 		user_id           = self.request.query_params.get('user_id',None)
# 		company_type      = self.request.query_params.get('company_type',None)


# # >>>>>>> c5684d76124cf519f95b2307fd58a1c0b9817874
# 		if user_id is not None:
# 			queryset=queryset.filter(id=user_id)
# 		if company_id is not None:
# 			queryset=queryset.filter(company__id=company_id)
# 		if firm_id is not None:
# 			queryset = queryset.filter(firm=firm_id)
# 		if role_id is not None:
# 			# role_ids = json.loads(role_id)
# 			queryset = queryset.filter(roles = role_id)	
# 		if role_ids is not None:
# 			role_ids = json.loads(role_ids)
# 			queryset = queryset.filter(roles__in=role_ids)		

# 		if email is not None:
# 			queryset = queryset.filter(email__iexact=email)
# 		if mobile is not None:
# 			# print(mobile)
# 			queryset = queryset.filter(mobile__iexact=mobile)		
# 		if not_employee_id is not None:
# 			# print(not_employee_id)
# 			# print(email)
# 			queryset = queryset.filter(~Q(id=not_employee_id))
# 			# print(queryset.query)	
# 		if assigned_marketer is not None:
# 			assigned_marketer_list = json.loads(assigned_marketer)
# 			queryset = 	queryset.filter(~Q(id__in=assigned_marketer_list))
# 		if marketers is not None:
# 			marketers = json.loads(marketers)
# 			queryset = 	queryset.filter(Q(id__in=marketers))
# 		if company_type is not None:
# 			queryset=queryset.filter(company__company_type__iexact=(company_type.lower()))	
# 		if search is not None:
# 			search = search.strip()
# 			queryset = queryset.filter(Q(emp_name__icontains=search)|Q(mobile__icontains=search)|Q(day_cost__icontains=search)|Q(address__icontains=search)|Q(firm__company__icontains=search)|Q(email=search)|Q(roles__name=search)|Q(company__company_name=search)|Q(l_marketer__marketers__emp_name__icontains=search)).distinct()
# 		if not_employee is not None:
# 			not_employee=json.loads(not_employee)
# 			queryset=queryset.filter(~Q(id__in=not_employee))
# 		if sort_by=='asc':
# 			return queryset.order_by('-'+collumn_name)
# 		elif sort_by=='dec': 
# 			return  queryset.order_by(''+collumn_name)
# 		else:
# 			return queryset.order_by('-added_date')
		return queryset

class applicant_list(generics.ListCreateAPIView):
	serializer_class = ApplicantSerializer
	queryset = Applicant.objects.all()		
	def get_queryset(self):		
		queryset          = Applicant.objects.all()
		return queryset

class show(generics.RetrieveUpdateDestroyAPIView):
	queryset = Applicant.objects.all()
	serializer_class = ApplicantSerializer

class change_password(generics.RetrieveUpdateDestroyAPIView):
	queryset = Applicant.objects.all()
	serializer_class = ApplicantPasswordSerializer
	