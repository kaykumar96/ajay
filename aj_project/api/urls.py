from django.conf.urls import url,include
from .views import login, applicant, payment
# from api import update_calltable

from django.conf import settings
from django.conf.urls.static import static

urlpatterns =[
			url(r'^1.0/',
				include([
					url(r'^apilogin/$', login.login.as_view(), name='login'),
					#url(r'^register/$', register.register.as_view(), name='register')

					url(r'^register/list/$',applicant.list.as_view(), name="registration"),
					url(r'^applicant/list/$',applicant.applicant_list.as_view(),name="applicant_list"),				
					url(r'^applicant/show/(?P<pk>[0-9]+)$',applicant.show.as_view(),name="applicant_show"),	

					url(r'^payment/list/$',payment.list.as_view(),name="payment_list"),			


				]))		
			]