from django.db import models
import pytz
# Create your models here.


class Applicant(models.Model):
    id              = models.AutoField(primary_key=True)
    roll_no         = models.CharField(max_length=150, unique=True)
    name            = models.CharField(max_length=150)
    d_o_b           = models.DateTimeField()
    email           = models.EmailField(max_length=150)
    mobile_no       = models.EmailField(max_length=150)
    profile_image   = models.FileField(upload_to='profile_image/',null=True,blank=True)
    sign_image      = models.FileField(upload_to='sign_image/',null=True,blank=True)
    form_number     = models.CharField(max_length=150, null=True, blank=True)
    addmission_date = models.DateTimeField(null=True, blank=True)	
    sec             = models.CharField(max_length=150, null=True, blank=True)
    app_date        = models.DateTimeField(null=True, blank=True)
    c_address       = models.CharField(max_length=150, null=True, blank=True)
    c_district      = models.CharField(max_length=150, null=True, blank=True)
    c_pin           = models.CharField(max_length=150, null=True, blank=True)
    c_state         = models.CharField(max_length=150, null=True, blank=True)
    category        = models.CharField(max_length=150, null=True, blank=True)
    class_roll_no   = models.CharField(max_length=150, null=True, blank=True)
    college_code    = models.CharField(max_length=150, null=True, blank=True)
    college_name    = models.CharField(max_length=150, null=True, blank=True)
    core1           = models.CharField(max_length=150, null=True, blank=True)
    core2           = models.CharField(max_length=150, null=True, blank=True)
    core3           = models.CharField(max_length=150, null=True, blank=True)
    core4           = models.CharField(max_length=150, null=True, blank=True)
    course          = models.CharField(max_length=150, null=True, blank=True)
    dse1            = models.CharField(max_length=150, null=True, blank=True)
    dse2            = models.CharField(max_length=150, null=True, blank=True)
    ge1             = models.CharField(max_length=150, null=True, blank=True)
    result_status   = models.CharField(max_length=150, null=True, blank=True)
    gender          = models.CharField(max_length=150, null=True, blank=True)
    is_cg_domicile  = models.BooleanField(null=True, blank=True)
    m_name          = models.CharField(max_length=150, null=True, blank=True)
    p_address       = models.CharField(max_length=150, null=True, blank=True)
    p_district      = models.CharField(max_length=150, null=True, blank=True)
    p_pin           = models.CharField(max_length=150, null=True, blank=True)
    p_state         = models.CharField(max_length=150, null=True, blank=True)
    reg_no          = models.CharField(max_length=150, null=True, blank=True)
    reg_year        = models.CharField(max_length=150, null=True, blank=True)
    session         = models.CharField(max_length=150, null=True, blank=True)
    stream           = models.CharField(max_length=150, null=True, blank=True)

    class Meta:
        db_table='applicant'
        # unique_together =  ('current_mobile','firm')

    def __str__(self):
        return '%s' % self.name



class Payment(models.Model):
    id               = models.AutoField(primary_key=True)
    applicant_id     = models.ForeignKey(Applicant, on_delete=models.SET_NULL, blank=True, null=True)
    payment_amount   = models.CharField(max_length=150, null=True, blank=True)
    payment_date     = models.DateTimeField(null=True, blank=True)
    payment_status   = models.CharField(max_length=150, null=True, blank=True)
    reference_txn_id = models.CharField(max_length=150, null=True, blank=True)
    sem              = models.CharField(max_length=150, null=True, blank=True)

    class Meta:
        db_table = 'payment'

    def __str__(self):
        return '%s'	% self.reference_txn_id




